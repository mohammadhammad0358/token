pragma solidity >=0.6.0 <0.8.0;

import "./contracts/token/ERC20/ERC20.sol";
import "./contracts/access/AccessControl.sol";

contract SwissReal is ERC20,AccessControl {
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    string private constant _name = "SwissReal";
    string private constant _symbol="SXRe";
    uint256 private constant _initialBalance = 200000000;
    uint8 private constant _decimals=6;

    constructor () public  ERC20(_name, _symbol) {
        _mint(msg.sender, _initialBalance);
        _setupDecimals(_decimals);
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
    }

    function mint(address account, uint256 amount) public {
        require(hasRole(MINTER_ROLE, account));
        _mint(account, amount);
    }

}
